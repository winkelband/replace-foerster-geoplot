#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Replace dummy values in Foerster Ferex datasets

Just run the script with
    $ python main.py

You may adjust the variables `old_value` and `new_value` (old_values needs to get replaced by new_value).

2017-06-30
"""
__author__ = "David Berthel"
__license__ = "Public Domain: The Unlicense"
__version__ = "1.0"
__email__ = "mail@davidberthel.de"

from tkinter.filedialog import askopenfilenames, askdirectory
from tkinter import messagebox, Label, Entry, Button, Tk
from tkinter.messagebox import askyesno
import csv
import os


# set value to replace
old_value = '1.797E+308'
new_value = '2047.50'


# dialog for user input (suffix)
class PopupWindow(object):
    def __init__(self, master):
        top = self.top = master
        self.l = Label(top, text="Please enter suffix:")
        self.l.pack()
        self.e = Entry(top)
        self.e.pack()
        self.b = Button(top, text='Ok', command=self.cleanup)
        self.b.pack()
        self.value = None

    def cleanup(self):
        self.value = self.e.get()
        self.top.destroy()
        self.top.quit()
        print ("suffix:", self.value)


def load_file():
    # ask for files to process
    file_names = askopenfilenames(
        title="Select file",
        filetypes=(("XYZ files", "*.xyz"), ("All files", "*.*"))
    )

    if file_names:
        # ask for output directory
        output_path = askdirectory(title='Please select an output directory')
        print ("output directory (path)", output_path)

        if output_path:
            # ask for a suffix to append to changed files
            view = Tk()
            user_input = PopupWindow(view)
            view.mainloop()

            suffix = user_input.value
            if not suffix:
                suffix = ""
            changed_count = 0

            # pass every single file with its path to function process_file and count per file +1 to `changed_count` if `old_value` got replaced
            for file_name in file_names:
                nothing_changed = process_file(file_name, output_path, suffix)
                print ('changed={}'.format(nothing_changed), "for file:", file_name)
                if not nothing_changed:
                    changed_count += 1

            # show info when all tasks done, total number of processed files and total number of changed files
            messagebox.showinfo("info", str(changed_count) + "/" + str(len(file_names)) + " files were changed. "
                                + str(len(file_names)) + " files written.")


# create new files (add suffix), load old files, replace string, save old files with changes in new files, files without substitutions are only copied to output directory
def process_file(file_name, output_path, suffix):
        # create new filename in output directory, add suffix
        name, extension = os.path.splitext(os.path.basename(file_name))
        file_name_new = os.path.join(output_path, name + suffix + extension)

#        print(os.path.normpath(file_name))
#        print(os.path.normpath(file_name_new))

        if os.path.normpath(file_name) == os.path.normpath(file_name_new):
            if not askyesno("Conflict", "File '" + os.path.normpath(file_name) + "' does already exist. Override?"):
                return True

        nothing_changed = True

        # load old file (csv) in read mode, tab as delimiter
        old = []
        with open(file_name, 'r') as csv_file_in:
            reader = csv.reader(csv_file_in, delimiter='\t')
            for row in reader:
                old.append(row)

            # create new file (csv), tab as delimiter
        with open(file_name_new, 'w') as csv_file_out:
            writer = csv.writer(csv_file_out, delimiter='\t', lineterminator='\n')

            # copy every line from old file to new file and replace '1.797E+308' with '2047.50' in third column if `nothing_changed` is 'False'
            for row in old:
                if row[2] and str(old_value) in row[2]:
                    nothing_changed = False
                    row[2] = row[2].replace(str(old_value), str(new_value))

                writer.writerow(row)

        # if no substitutions (`nothing_changed` is 'True') have to be done copy old file with old file name to output directory
        # use commented line if suffix should be also provided for not changed files
        if nothing_changed:
            os.rename(file_name_new, os.path.join(output_path, os.path.basename(file_name)))
#            os.rename(file_name_new, os.path.join(output_path, name + suffix +extension))

        return nothing_changed


if __name__ == "__main__":
    load_file()
